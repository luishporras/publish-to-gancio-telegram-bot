# Telegram bot to publish to a Gancio instance
This repo is using python and python-telegram-bot(v20.0.3) library to push events to an instance.

## Resources
This bot is based on the [conversation example](https://docs.python-telegram-bot.org/en/stable/examples.conversationbot.html) from the library

## License
This code is licensed under GPLv3.0 or later